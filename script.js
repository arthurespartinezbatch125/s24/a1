//Add the following users:
db.users.insertMany(
	[
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"email": "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"email": "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firreli",
			"email": "jfirreli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"email": "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"email": "pcastillo@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"email": "dgvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}
	]
)
//Add the following courses
db.courses.insertMany(
	[
		{
			"name": "Professional Development",
			"price": 10000
		},
		{
			"name": "Business Processing",
			"price": 13000
		}
	]
)
//Get user ID's and add them as enrolless of the courses
db.courses.updateOne(
	{
		"name": "Professional Development"
	},
	{
		$set:{"Enrollees": [ObjectId("6125b48f07087aac8c8cd0e4"),ObjectId("6125b48f07087aac8c8cd0e6")]}
	}

)
db.courses.updateOne(
	{
		"name": "Business Processing"
	},
	{
		$set:{"Enrollees": [ObjectId("6125b48f07087aac8c8cd0e7"),ObjectId("6125b48f07087aac8c8cd0e5")]}
	}
)
//Get users who are not administrator
db.users.find(
	{"isAdmin": false}
)